<?php
require '../functions.php';

$keyword = $_GET["keyword"];

$query =  "SELECT * FROM sepatu
            WHERE
            nama LIKE '%$keyword%' OR
            brand LIKE '%$keyword%' OR
            kode_sepatu LIKE '%$keyword%' OR
            jenis LIKE '%$keyword%'
        ";

$sepatu = query($query);

?>

<table border="1" cellpadding="10" cellspacing="0">
    <tr>
        <th>No.</th>
        <th>Gambar</th>
        <th>Nama</th>
        <th>Brand</th>
        <th>Kode Sepatu</th>
        <th>Jenis</th>
        <th>Aksi</th>
    </tr>
    <?php $i = 1; ?>
    <?php foreach ($sepatu as $row) : ?>
        <tr>
            <td><?= $i; ?></td>
            <td><img src="../pertemuan6/img/images/<?= $row["gambar"]; ?>" width="50" alt=""></td>
            <td><?= $row["nama"]; ?></td>
            <td><?= $row["brand"]; ?></td>
            <td><?= $row["kode_sepatu"]; ?></td>
            <td><?= $row["jenis"]; ?></td>
            <td>
                <a href="ubah.php?id=<?= $row["id"]; ?>">Ubah</a> |
                <a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin?');">Hapus</a>
            </td>
        </tr>
        <?php $i++; ?>
    <?php endforeach; ?>
</table>