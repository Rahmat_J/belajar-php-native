<?php
// koneksi ke database

$conn = mysqli_connect("localhost", "root", "", "phpdasar");


// ambil data dari tabel sepatu / query data sepatu
$result = mysqli_query($conn, "SELECT * FROM sepatu");

// untuk buat validasi ngecek ada gag tabelnya
// if (!$result) {
//     echo mysqli_error($conn);
// }

// ambil data (fetch) sepatu dari object result
// mysqli_fetch_row() // mengembalikan array numeric
// mysqli_fetch_assoc() //mengembalikan array associative
// mysqli_fetch_array() // mengembalikan keduanya
// mysqli_fetch_object()

// while (
//     $spt = mysqli_fetch_assoc($result)
// ) {
//     var_dump($spt);
// }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
</head>

<body>
    <h1>Daftar Sepatu</h1>
    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Aksi</th>
            <th>Gambar</th>
            <th>Nama</th>
            <th>Brand</th>
            <th>Kode Sepatu</th>
            <th>Jenis</th>
        </tr>
        <?php $i = 1; ?>
        <?php while ($row = mysqli_fetch_assoc($result)) : ?>
            <tr>
                <td><?= $i; ?></td>
                <td>
                    <a href="">Ubah</a> |
                    <a href="">Hapus</a>
                </td>
                <td><img src="../pertemuan6/img/images/<?= $row["gambar"]; ?>" width="50" alt=""></td>
                <td><?= $row["nama"]; ?></td>
                <td><?= $row["brand"]; ?></td>
                <td><?= $row["kode_sepatu"]; ?></td>
                <td><?= $row["jenis"]; ?></td>
            </tr>
            <?php $i++; ?>
        <?php endwhile; ?>
    </table>
</body>

</html>