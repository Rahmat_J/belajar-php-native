<?php
// koneksi ke DBMS
$conn = mysqli_connect("localhost", "root", "", "phpdasar");


// cek apakah tombol submit sudah ditekan atau belum
if (isset($_POST["submit"])) {
    // var_dump($_POST);


    // ambil data dari tiap elemen dalam form
    $nama = $_POST["nama"];
    $brand = $_POST["brand"];
    $kode_sepatu = $_POST["kode_sepatu"];
    $jenis = $_POST["jenis"];
    $gambar = $_POST["gambar"];


    // query insert data  
    $query = "INSERT INTO sepatu
                VALUES
                ('', '$nama','$brand','$kode_sepatu','$jenis','$gambar')
                ";
    mysqli_query($conn, $query);

    // cek apakah data berhasil di tambahkan atau tidak
    // var_dump(mysqli_affected_rows($conn));
    if(mysqli_affected_rows($conn) > 0) {
        echo "Berhasil";
    } else {
        echo "Gagal";
        echo "<br>";
        echo mysqli_error($conn);
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Data Sepatu</title>
</head>

<body>
    <h1>Tambah Data Sepatu</h1>

    <form method="post" action="">
        <ul>
            <li>
                <label for="nama">Nama :</label>
                <input type="text" name="nama" id="nama">
            </li>
            <li>
                <label for="brand">Brand :</label>
                <input type="text" name="brand" id="brand">
            </li>
            <li>
                <label for="kode_sepatu">Kode Sepatu :</label>
                <input type="text" name="kode_sepatu" id="kode_sepatu">
            </li>
            <li>
                <label for="jenis">Jenis :</label>
                <input type="text" name="jenis" id="jenis">
            </li>
            <li>
                <label for="gambar">Gambar :</label>
                <input type="text" name="gambar" id="gambar">
            </li>
            <li>
                <button type="submit" name="submit">Tambah Data!</button>
            </li>
        </ul>
    </form>
</body>

</html>