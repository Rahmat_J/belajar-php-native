<?php

// Array Associative
// Definisinya sama seperti array numeric, kecuali
// key-nya adalah string yang kita buat sendiri
$mahasiswa = [[
    "nama" => "Rahmat Januardi",
    "nrp" => "1423123132",
    "email" => "rahmatjanuardi@gmail.com",
    "jurusan" => "teknik komputer",
    "gambar" => "adit.jpg"
], [
    "nama" => "Sri Wulandari",
    "nrp" => "32428348",
    "email" => "sriw@gmail.com",
    "jurusan" => "Fisikawan",
    "gambar" => "jarwo.jpg"
]];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Daftar Mahasiswa</h1>

    <?php foreach ($mahasiswa as $mhs) : ?>
        <ul>
            <li><img src="img/<?= $mhs["gambar"] ?>" alt=""></li>
            <li>Nama: <?= $mhs["nama"]; ?></li>
            <li>NRP: <?= $mhs["nrp"]; ?></li>
            <li>Jurusan: <?= $mhs["jurusan"]; ?></li>
            <li>EMail: <?= $mhs["email"]; ?></li>
        </ul>
    <?php endforeach; ?>
</body>

</html>