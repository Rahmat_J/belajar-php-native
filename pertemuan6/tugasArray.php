<?php

// Array Associative
// Definisinya sama seperti array numeric, kecuali
// key-nya adalah string yang kita buat sendiri
$sepatu = [[
    "nama" => "Nike Air Jordan",
    "brand" => "Nike",
    "kode_sepatu" => "1235948",
    "jenis" => "sport",
    "gambar" => "airjordan.jpg"
], [
    "nama" => "Nike Zoom",
    "brand" => "Nike",
    "kode_sepatu" => "1841232",
    "jenis" => "kasual",
    "gambar" => "nike_zoom.jpg"
], [
    "nama" => "Nike MSL",
    "brand" => "Nike",
    "kode_sepatu" => "8412371",
    "jenis" => "sport",
    "gambar" => "niki_msl.jpg"
], [
    "nama" => "Nike Winflo",
    "brand" => "Nike",
    "kode_sepatu" => "743247",
    "jenis" => "kasual",
    "gambar" => "niki_winflo.jpg"
], [
    "nama" => "Nike Red Lunar",
    "brand" => "Nike",
    "kode_sepatu" => "2349834",
    "jenis" => "sport",
    "gambar" => "red_lunar.jpg"
], [
    "nama" => "Adidas Ultraboost",
    "brand" => "Adidas",
    "kode_sepatu" => "932842034",
    "jenis" => "sport",
    "gambar" => "adidas1.jpg"
], [
    "nama" => "Adidas Gazelle",
    "brand" => "Adidas",
    "kode_sepatu" => "90123823",
    "jenis" => "sport",
    "gambar" => "adidas2.jpg"
], [
    "nama" => "Adidas Unisex Originals Superstar",
    "brand" => "Adidas",
    "kode_sepatu" => "912334824",
    "jenis" => "sport",
    "gambar" => "adidas3.jpg"
], [
    "nama" => "Adidas Samba",
    "brand" => "Adidas",
    "kode_sepatu" => "97591382",
    "jenis" => "sport",
    "gambar" => "adidas4.jpg"
], [
    "nama" => "Adidas Supernova",
    "brand" => "Adidas",
    "kode_sepatu" => "991238123",
    "jenis" => "sport",
    "gambar" => "adidas5.jpg"
]];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produk Sepatu</title>
</head>

<body>
    <h1>Dafta Sepatu</h1>

    <?php foreach ($sepatu as $spt) : ?>
        <ul>
            <li><img src="img/images/<?= $spt["gambar"] ?>" alt=""></li>
            <li>Nama Sepatu: <?= $spt["nama"]; ?></li>
            <li>Brang: <?= $spt["brand"]; ?></li>
            <li>Kode Sepatu: <?= $spt["kode_sepatu"]; ?></li>
            <li>Jenis: <?= $spt["jenis"]; ?></li>
        </ul>
    <?php endforeach; ?>
</body>

</html>