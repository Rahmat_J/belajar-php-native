<?php

echo date('d-M-Y, l');
echo "<br>";
echo time();
echo "<br>";
echo date("l", time() + 60 * 60 * 24 * 100);
echo "<br>";
echo date("l, d M Y", time() - 60 * 60 * 24 * 100); //60 detik di kali 60 menit dikali 24 jam di kali 100 hari
echo "<br>";
// mktime(0,0,0,0,0,0); // jam, menit, detik, bulan, tanggal, tahun
echo date("l", mktime(0, 0, 0, 1, 25, 1996));
echo "<br>";
// strtotime
echo date("l", strtotime("25 jan 1996"));
echo "<br>";
echo substr(date("Y"),2,2);
echo "<br>";
echo date("y");