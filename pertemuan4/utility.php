<?php
// var_dump()
$a = 32;
echo var_dump($a) . "<br>";

$b = "hello_world";
echo var_dump($b) . "<br>";

$c = 32.5;
echo var_dump($c) . "<br>";

$d = array("red", "green", "blue");
echo var_dump($d) . "<br>";

$e = array(32, "Hello world!", 32.5, array("red", "green", "blue"));
echo var_dump($e) . "<br>";

// Dump two variables
echo var_dump($a, $b) . "<br>";

echo "<br>";
echo "<br>";





// isset() - untuk mengecek isi dari variabel, jika isi variabe tidak null makan akan di tampilkan dan jika null tidak akan di tampilkan
$a = 0;
// True because $a is set
if (isset($a)) {
    echo "Variable 'a' is set.<br>";
}

$b = 2;
// False because $b is NULL
if (isset($b)) {
    echo "Variable 'b' is set.";
}
echo "<br>";
echo "<br>";




// empty()
$c = 0;

echo empty($c) . "<br>";

// True because $a is empty
if (empty($c)) {
    echo "Variable 'c' is empty.<br>";
}

// True because $a is set
if (isset($c)) {
    echo "Variable 'c' is set";
}
echo "<br>";
echo "<br>";




// die()
$site = "https://www.w3schools.com/";
fopen($site, "r")
    or die("Unable to connect to $site");

echo "<br>";
echo "<br>";




// sleep()
echo date('h:i:s') . "<br>";

//sleep for 3 seconds
sleep(2);

//start again
echo date('h:i:s');


echo "<br>";
echo "<br>";
