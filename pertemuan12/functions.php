<?php
// koneksi ke database

$conn = mysqli_connect("localhost", "root", "", "phpdasar");

function query($query)
{
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}

function tambah($data)
{
    global $conn;
    // ambil data dari tiap elemen dalam form
    $nama = htmlspecialchars($data["nama"]);
    $brand = htmlspecialchars($data["brand"]);
    $kode_sepatu = htmlspecialchars($data["kode_sepatu"]);
    $jenis = htmlspecialchars($data["jenis"]);
    $gambar = htmlspecialchars($data["gambar"]);

    // query insert data  
    $query = "INSERT INTO sepatu
                VALUES
                ('', '$nama','$brand','$kode_sepatu','$jenis','$gambar')
                ";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function hapus($id)
{
    global $conn;
    mysqli_query($conn, "DELETE FROM sepatu WHERE id = $id");
    return mysqli_affected_rows($conn);
}

function ubah($data)
{
    global $conn;

    $id = $data["id"];
    $nama = htmlspecialchars($data["nama"]);
    $brand = htmlspecialchars($data["brand"]);
    $kode_sepatu = htmlspecialchars($data["kode_sepatu"]);
    $jenis = htmlspecialchars($data["jenis"]);
    $gambar = htmlspecialchars($data["gambar"]);

    $query = "UPDATE sepatu SET
                    nama = '$nama',
                    brand = '$brand',
                    kode_sepatu = '$kode_sepatu',
                    jenis = '$jenis',
                    gambar = '$gambar'
                    WHERE id = $id
                ";
    mysqli_query($conn, $query);

    return mysqli_affected_rows($conn);
}

function cari($keyword)
{
    $query = "SELECT * FROM sepatu
        WHERE
        nama LIKE '%$keyword%' OR
        brand LIKE '%$keyword%' OR
        kode_sepatu LIKE '%$keyword%' OR
        jenis LIKE '%$keyword%'
    ";

    return query($query);
}
