<?php

session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

require 'functions.php';

// ambil data di URL
$id = $_GET["id"];

// query data mahasiswa berdasarkan id
$spt = query("SELECT * FROM sepatu WHERE id=$id")[0];


// cek apakah tombol submit sudah ditekan atau belum
if (isset($_POST["submit"])) {

    // cek apakah data berhasil di ubah atau tidak
    if (ubah($_POST) > 0) {
        echo "
            <script>
                alert('data berhasil diubah!');
                document.location.href = 'index.php';
            </script>
        ";
    } else {
        echo "
            <script>
                alert('data berhasil diubah!');
                document.location.href = 'index.php';
            </script>
        ";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah Data Sepatu</title>
</head>

<body>
    <h1>Ubah Data Sepatu</h1>

    <form method="post" action="" enctype="multipart/form-data">
        <input type="hidden" name="id" id="id" value="<?= $spt["id"]; ?>">
        <input type="hidden" name="gambarLama" value="<?= $spt["gambar"]; ?>">
        <ul>
            <li>
                <label for="nama">Nama :</label>
                <input type="text" name="nama" id="nama" required value="<?= $spt["nama"]; ?>">
            </li>
            <li>
                <label for="brand">Brand :</label>
                <input type="text" name="brand" id="brand" required value="<?= $spt["brand"]; ?>">
            </li>
            <li>
                <label for="kode_sepatu">Kode Sepatu :</label>
                <input type="text" name="kode_sepatu" id="kode_sepatu" required value="<?= $spt["kode_sepatu"]; ?>">
            </li>
            <li>
                <label for="jenis">Jenis :</label>
                <input type="text" name="jenis" id="jenis" required value="<?= $spt["jenis"]; ?>">
            </li>
            <li>
                <label for="gambar">Gambar :</label><br>
                <img src="../pertemuan6/img/images/<?= $spt["gambar"]; ?>" width="40" alt="">
                <input type="file" name="gambar" id="gambar">
            </li>
            <li>
                <button type="submit" name="submit">Ubah Data!</button>
            </li>
        </ul>
    </form>
</body>

</html>