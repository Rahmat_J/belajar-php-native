<?php

require_once __DIR__ . '/vendor/autoload.php';

require 'functions.php';

$sepatu = query("SELECT * FROM sepatu");


$mpdf = new \Mpdf\Mpdf();

$html = '
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/print.css">
</head>
<body>
    <p>Daftar Sepatu</p>
    <table border="1" cellpadding="10" cellspacing="0">
        <tr>
            <th>No.</th>
            <th>Gambar</th>
            <th>Nama</th>
            <th>Brand</th>
            <th>Kode Sepatu</th>
            <th>Jenis</th>
        </tr>';

$i = 1;
foreach ($sepatu as $row) {
    $html .= '<tr>
            <td>' . $i++ . '</td>
            <td><img src="../pertemuan6/img/images/' . $row["gambar"] . '" width="50"></td>
            <td>' .  $row["nama"] . '</td>
            <td>' .  $row["brand"] . '</td>
            <td>' .  $row["kode_sepatu"] . '</td>
            <td>' .  $row["jenis"] . '</td>
            </tr>';
}

$html .=  '</table>
</body>
</html>
';
$mpdf->WriteHTML('<h1>Hello world!</h1>');
$mpdf->WriteHTML($html);
$mpdf->Output('daftar-sepatu.pdf', \Mpdf\Output\Destination::INLINE);
