<?php
session_start();

if (!isset($_SESSION["login"])) {
    header("Location: login.php");
    exit;
}

require 'functions.php';

$sepatu = query("SELECT * FROM sepatu");

// tombol cari ditekan
if (isset($_POST["cari"])) {
    $sepatu = cari($_POST["keyword"]);
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Admin</title>
    <style>
        .loader {
            width: 100px;
            position: absolute;
            top: 135px;
            left: 300px;
            z-index: -1;
            display: none;
        }

        @media print {

            .logout,
            .tambah,
            .form-cari,
            .aksi {
                display: none;
            }
        }
    </style>
    <script src="js/jquery-3.6.0.min.js"></script>
</head>

<body>

    <a href="logout.php" class="logout">Logout</a> | <a href="cetak.php" target="_blank">Cetak</a>

    <h1>Daftar Sepatu</h1>

    <a href="tambah.php" class="tambah">Tambah data sepatu</a>
    <br><br>

    <form method="post" action="" class="form-cari">

        <input type="text" name="keyword" size="40" autofocus placeholder="Masukkan Keyword Pencarian" autocomplete="off" id="keyword">
        <button type="submit" name="cari" id="tombol-cari">Cari!</button>

        <img src="img/loader.gif" alt="" class="loader">

    </form>
    <br><br>

    <div id="container">
        <table border="1" cellpadding="10" cellspacing="0">
            <tr>
                <th>No.</th>
                <th>Gambar</th>
                <th>Nama</th>
                <th>Brand</th>
                <th>Kode Sepatu</th>
                <th>Jenis</th>
                <th class="aksi">Aksi</th>
            </tr>
            <?php $i = 1; ?>
            <?php foreach ($sepatu as $row) : ?>
                <tr>
                    <td><?= $i; ?></td>
                    <td><img src="../pertemuan6/img/images/<?= $row["gambar"]; ?>" width="50" alt=""></td>
                    <td><?= $row["nama"]; ?></td>
                    <td><?= $row["brand"]; ?></td>
                    <td><?= $row["kode_sepatu"]; ?></td>
                    <td><?= $row["jenis"]; ?></td>
                    <td class="aksi">
                        <a href="ubah.php?id=<?= $row["id"]; ?>">Ubah</a> |
                        <a href="hapus.php?id=<?= $row["id"]; ?>" onclick="return confirm('yakin?');">Hapus</a>
                    </td>
                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>
        </table>
    </div>
    <script src="js/script.js"></script>
</body>

</html>