<?php
// cek apakah tidak ada data di $_GET
if (
    !isset($_GET["nama"]) ||
    !isset($_GET["brand"]) ||
    !isset($_GET["kode_sepatu"]) ||
    !isset($_GET["jenis"]) ||
    !isset($_GET["gambar"])
) {
    // redirect
    header("Location: latihan1.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Daftar Sepatu</title>
</head>

<body>
    <ul>
        <li><img src="../pertemuan6/img/images/<?= $_GET["gambar"]; ?>"></li>
        <li><?= $_GET["nama"]; ?></li>
        <li><?= $_GET["brand"]; ?></li>
        <li><?= $_GET["kode_sepatu"]; ?></li>
        <li><?= $_GET["jenis"]; ?></li>
    </ul>

    <a href="latihan1.php">Back</a>
</body>

</html>