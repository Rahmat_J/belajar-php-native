<?php
// array
// variabel yang dapat memiliki banyak nilai
// elemen pada array boleh memiliki type data yang berbeda
// pasangan antara key dan value
// key-nya dimulai dari 0

// cara lama
$hari = array("senin", "selasa", "rabu");

// cara baru
$bulan = ["januari", "februari", "april"];
$arr1 = [123, "hello", false];


//menampilkan array
// var_dump() atau print_r()

// var_dump($hari);
// echo "<br>";

// print_r($bulan);
// echo "<br>";

// echo $hari[0] . "<br>";
// echo $arr1[1];

var_dump($hari);
$hari[] = "Kamis";
$hari[] = "jumat";
echo "<br>";
var_dump($hari);
